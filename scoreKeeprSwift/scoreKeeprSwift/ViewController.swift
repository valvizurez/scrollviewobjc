//
//  ViewController.swift
//  scoreKeeprSwift
//
//  Created by Victoria Alvizurez on 9/5/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var team1Lbl: UILabel!
    @IBOutlet var team2Lbl: UILabel!
    
    @IBOutlet var team1Stepper: UIStepper!
    @IBOutlet var team2Stepper: UIStepper!
    
    @IBAction func team1StepperChanged(_ sender: Any) {
        let myString = String(format: "%.0f", team1Stepper.value)
        team1Lbl.text = myString
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func resetBtnTouched(_ sender: Any) {
        //lables to zero
        //steppers to zero
        
        team1Stepper.value = 0;
        team1Lbl.text = "0";
        
        team2Stepper.value = 0;
        team2Lbl.text = "0";
        
    }
}

