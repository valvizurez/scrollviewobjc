//
//  ViewController.swift
//  scrollViewSwift
//
//  Created by Victoria Alvizurez on 9/7/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
 
    
    override func viewDidLoad() {
      
        super.viewDidLoad()
        
        //create scrollView
        let scrollView = UIScrollView(frame: self.view.bounds)
        scrollView.isPagingEnabled = true
        scrollView.backgroundColor = UIColor.blue
        self.view.addSubview(scrollView)
        
        //create 9x9 Grid
        scrollView.contentSize = CGSize(width: self.view.bounds.size.width * 3, height: self.view.bounds.size.height * 3)
        
        let height = self.view.bounds.size.height
        let width = self.view.bounds.size.width
        
        //left middle view
        let view = UIView(frame:CGRect(x:0,y:self.view.bounds.size.height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view.backgroundColor = UIColor.red
        scrollView.addSubview(view)
        
        //top middle view
        let view2 = UIView(frame:CGRect(x:width,y:1, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view2.backgroundColor = UIColor.green
        scrollView.addSubview(view2)
        
        //middle right view
        let view3 = UIView(frame:CGRect(x:width ,y:1, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view2.backgroundColor = UIColor.purple
        scrollView.addSubview(view3)
        
        //left bottom view
        let view4 = UIView(frame:CGRect(x:0 ,y:height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view4.backgroundColor = UIColor.yellow
        scrollView.addSubview(view4)
        
        //top left view
        let view5 = UIView(frame:CGRect(x:0 ,y:1, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view5.backgroundColor = UIColor.cyan
        scrollView.addSubview(view5)
        
        //right middle view
        let view6 = UIView(frame:CGRect(x:width * 2 ,y: height, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view6.backgroundColor = UIColor.orange
        scrollView.addSubview(view6)
        
        //right bottom view
        let view7 = UIView(frame:CGRect(x:width * 2 ,y: height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view7.backgroundColor = UIColor.darkGray
        scrollView.addSubview(view7)

        //middle bottom view
        let view8 = UIView(frame:CGRect(x:width ,y: height * 2, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view8.backgroundColor = UIColor.brown
        scrollView.addSubview(view8)
        
        //top right view
        let view9 = UIView(frame:CGRect(x:width * 2 ,y: 1, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        view9.backgroundColor = UIColor.black
        scrollView.addSubview(view9)
        //comment
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

